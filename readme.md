# TP Python 

## Etapes premilinaires
1. Télécharger ce dossier 
2. Activate l'environnement utilisé dans le cours précédent
3. Installer les packages suivants:
```conda install scikit-learn ```
```conda install pandas ```
4. Tester le code avec 
```
 python .\main.py --experiment "titanic" --lr 1e-2 --seed 1234
```

#Exercices:
## Numpy pour KNN
L’algorithme des k plus proches voisins s'écrit en abrégé k-NN ou KNN , de l'anglais k-nearest
neighbors, appartient à la famille des algorithmes d’apprentissage automatique sans paramètres.

L’algorithme des k plus proches voisins est un algorithme d’apprentissage supervisé, il est
nécessaire d’avoir des données labellisées. À partir d’un ensemble **E** de données labellisées (données d'entraînement), il sera
possible de classer (déterminer le label) d’une nouvelle ensemble de donnée **T** (donnée n’appartenant pas à E, données de teste).

#### Principe de algorithme
On suppose que l'ensemble **E** contiennent *n* données labellisées et **T**, une autre ensemble des données
disjointe de **E** qui ne possède pas de label. Soit *d* une fonction qui renvoie la distance
(qui reste à choisir) entre la donnée *u* de **T** et une donnée quelconque appartenant à **E**. Soit un entier k
inférieur ou égal à *n*.

Le principe de l’algorithme de k-plus proches voisins est le suivant :

Pour chaque donnée *u* dans **T**:
1. On calcule les distances (par exemple Euclidean) entre la donnée *u* et chaque donnée appartenant à **E**.
2. On retient les k données du jeu de données E les plus proches de *u*.
3. On attribue à *u* la classe qui est la plus fréquente parmi les k données les plus proches.

### A faire
Coder l'algorithme KNN à l'aide de Numpy 
1. Remplir la fonction ``KNN_algorithm`` dans le fichier **trainer.py**. 

   **Remarque** : Essayer de coder de façon avec moins de boucle (for), rappelle que numpy nous permet de faire les calculs terme à terme.  
2. Tester KNN avec la commande : `` python .\main.py --experiment "titanic" --knn  --epochs 1``
3. Modifier la valeur de *k* (fin de fichier **trainer.py**) et retester. Quelle est votre observation sur effect de *k* ?
Quelle est la meilleure valeur de *k* ?


## PyTorch et réseaux des neurones (Bonus)
1. Intégrer le modèle LeNet sur les données "fash_mnist", cherche la meilleure hyperparamètres/algorithme pour l'apprentissage (e.g., 
batch size, taux d'apprentissage, nombre d'epochs...)
2. Quelle est votre observation ? 
3. Quel est l'avantage d'utiliser le réseau de neurones convolutifs ?