##Corrigé d'exercice pour Titanic:
1. Ajouter dataset: (**loader.py**)


Gérer des données par classe (hérité de torch.utils.data.Dataset)
```python 
class TitanicData(torch.utils.data.Dataset):
    def __init__(self, nom_fichier, is_training=True):
    # Prétraitement simple des données à l'aide de pandas
        data = pd.read_csv(nom_fichier)
        # Conversion de catégorie au nombre discrete
        data.Sex = data.Sex.astype('category').cat.codes.astype("int8")
        data.Embarked = data.Embarked.astype('category').cat.codes.astype("int8")
        # Remplacer les valeur null par le moyenne
        data.Age.fillna(int(data.Age.mean()), inplace=True)
        data.Embarked.fillna(int(data.Embarked.mean()), inplace=True)
        # Rejeter les colonnes qu'on ne utilise pas
        df_train = data.drop(['Name', 'Ticket', 'Cabin', 'PassengerId'], axis=1)
        # Diviser les données en données d'entraînement et données de test
        ...
        
    def __len__(self):
        # Redéfinir la taille des données
        ...
    
    def __getitem__(self, indice):
        # Redéfinir les éléments retournés dans les données
```

Mettre à jour la fonction ``def get_loader(...):``. Elle retourne un **data loader** 
qui permet de parcourir les données efficaces. 

```python
    elif experiment_name == "titanic":
        dataset = TitanicData("data/titanic/train.csv", is_training=is_training)
    ...
    data_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=1)
    return data_loader
```

2. Ajouter le modèle : (**models.py**, on considère une regression logistique)
```python
def get_model(experiment_name, device):
    ...
    elif experiment_name == "titanic":
        model = nn.Sequential(
            LinearLayer(input_dim=7, output_dim=1, bias=True),
            nn.Sigmoid())

```

3. Ajouter la fonction de perte : (**trainer.py**)
```python
def get_trainer(...):
    ...
    elif experiment_name == "titanic":
        criterion = torch.nn.BCELoss().to(device) # Perte logistique 
        metric = accuracy_binary 
```

4. Ajouter la metric pour l'évaluation : (**metric.py**)
```python
def accuracy_binary(y_pred, y):
    """computes classification accuracy

    Parameters
    ----------
    y_pred: torch.tensor with size BZx1

    y: 1-D torch.torch.tensor with size BZx1

    Returns
    -------
        * float
    """
    predicted = (y_pred > 0.5).type(torch.float32)
    correct = (predicted == y).float()
    acc = correct.sum() / len(y)
    return acc

```

###Tester avec la commande : 


``
python .\main.py --experiment "titanic" --lr 1e-2 --seed 1234 
``